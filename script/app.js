﻿(function () {
    var module = angular.module("app", ["ngRoute"]);

    module.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "/html/home.html",
                controller: "homeController"
            })
            .when("/home", {
                templateUrl: "/html/home.html",
                controller: "homeController"
            })
            .when("/userresume/:username", {
                templateUrl: "/html/userResume.html",
                controller: "userResumeController"
            })
            .when("/user_repos", {
                templateUrl: "/files/repos.json"
            })
            .otherwise({ redirectTo: "/home" });
    }]);
})();